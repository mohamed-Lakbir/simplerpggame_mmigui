#include "fight_window.h"
#include "ui_fight_window.h"
#include "weapon.h"
#include "qpixmap.h"
#include "QDebug"
#include "QString"

/**
 * @brief Fight_Window::Fight_Window
 *
 */
Fight_Window::Fight_Window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Fight_Window)
{
    ui->setupUi(this);
    this->m_player=new Controller();
    ui->Healthbar_player->setValue(m_player->getBreed().getHealth());
    ui->Game_Over->hide();
    ui->Continue->hide();
    round=1;
    bossround=1;
    ui->Jiren->hide();
    ui->Jirenultrainstinct->hide();

}

Fight_Window::~Fight_Window()
{
    delete ui;
}

void Fight_Window::on_pushButton_clicked()
{
    this->close();
    emit this->i_am_finshed();
}

Controller *Fight_Window::getNpc() const
{
    return npc;
}

void Fight_Window::setNpc(Controller *value)
{
    npc = value;
}

/**
 * @brief Fight_Window::bossfightAction
 * @param bossfight Dieser bool Wert gibt wieder, ob es sich hier um einen bossfight handelt.
 * @details Diese Methode unterscheidet zwischen Bossfight und keinem Bossfight. Je nachdem,
 *          was hier vorliegt, entscheidet diese Methode, welche Werte und Bilder der npc Controller erhält.
 */
void Fight_Window::bossfightAction(bool bossfight)
{
    m_am_I_an_Boss=bossfight;
    if(bossfight==false){
    Breed breed("Undefine",50,100,0,0,0,12,Weapon("unarmed",0));
    this->npc=new Controller("noboss",false,breed);
    ui->Healthbar_enemy->setValue(npc->getBreed().getHealth());
    ui->Enemy_name->setText("SheepWarrior");
    ui->WhatisHappening->setText("Sheepman is attacking!");

    this->show();
    }else if(bossfight==true){
    Breed breed("Jiren",100,100,100,100,100,20,Weapon("unarmed",0));
    this->npc=new Controller("Jiren",false,breed);
    ui->Healthbar_enemy->setValue(npc->getBreed().getHealth());
    ui->Enemy_name->setText("Jiren");
    ui->WhatisHappening->setText("A dangerous fighter is in your way");
    this->show();
    }
}

Controller *Fight_Window::getPlayer() const
{
    return m_player;
}

void Fight_Window::setPlayer(Controller *player)
{
    m_player = player;
    ui->Healthbar_player->setValue(m_player->getBreed().getHealth());
}

bool Fight_Window::getAm_I_an_Boss() const
{
    return m_am_I_an_Boss;
}

void Fight_Window::setAm_I_an_Boss(bool am_I_an_Boss)
{
    m_am_I_an_Boss = am_I_an_Boss;
}

/**
 * @brief Fight_Window::on_Attack_clicked
 * @details Diese Methode, was passiert wenn der Player angreift.
 * @details Je nachdem ob es sich hier ein Bossfight handelt oder ein normaler Kampf, werdem dem Gegner
 *          Lebenspunkte abgezogen.
 * @details Wenn der Spieler unter 0 Lebenspunkte erhält, wird ein Gameoverscreen ausgelöst.
 * @details Diese Methode bestimmt auch was grade Passiert bzw gibt das weiter.
 */
void Fight_Window::on_Attack_clicked()
{
    if(m_am_I_an_Boss==false){
        npc->takedmg(m_player->getBreed().getAttack());
        ui->Healthbar_enemy->setValue(npc->getBreed().getHealth());
        if(npc->getBreed().getHealth()<0){
            round=1;
            ui->Continue->hide();
            ui->Attack->show();
            ui->sheeppic->hide();
            this->hide();
        }else{
            ui->Continue->show();
            ui->Attack->hide();
            ui->Heal->hide();
        }
        ui->WhatisHappening->setText("You start an attack");
        if(m_player->getBreed().getHealth()<0){
            ui->Game_Over->setText("Game Over, restart the game.");
        }
    }else{
        ui->WhatisHappening->setText("You start an attack, but its useless. He is too strong.");
        ui->Continue->show();
        ui->Attack->hide();
        ui->Heal->hide();
        ui->sheeppic->hide();
        ui->Jiren->show();
    }

}

/**
 * @brief Fight_Window::on_Heal_clicked
 * @details Diese Methode reagiert auf das klicken von dem Healbutton. Es heilt den Spieler und setzt seine
 *          Gesundheit wieder hoch.
 */
void Fight_Window::on_Heal_clicked()
{
    this->m_player->heal(100);
    ui->Healthbar_player->setValue(m_player->getBreed().getHealth());
    ui->WhatisHappening->setText("You are healing ur body");
    ui->Continue->show();
    ui->Attack->hide();
    ui->Heal->hide();

}

/**
 * @brief Fight_Window::on_Continue_clicked
 * @details Diese Methode bestimmt, was für ein Enemypic ausgestrahlt wird.
 */
void Fight_Window::on_Continue_clicked()
{

   if(m_am_I_an_Boss==false){
       sheepfight();
   }else{
       ui->sheeppic->hide();
       ui->Jiren->show();
       bossfight();
   }

   ui->Heal->show();
   ui->Attack->show();
   ui->Continue->hide();
}

/**
 * @brief Fight_Window::sheepfight
 * @details Das Kampfsystem verläuft nach einem Runden prinzip. Jede Weiter Runde macht der Gegner was anderes.
 * @details Sie greifen unter umständen auch den Player an und ziehem ihm Werte ab.
 */
void Fight_Window::sheepfight()
{
    if(round==1){
       m_player->takedmg(npc->attack());
       ui->WhatisHappening->setText("Sheep man attacks you");
       round++;
    }else if(round==2){
       m_player->takedmg(npc->attack());
       ui->WhatisHappening->setText("Sheep man attacks you");
       ui->Healthbar_player->setValue(m_player->getBreed().getHealth());
       round++;
    }else if(round==3){
        m_player->takedmg(npc->attack());
        ui->WhatisHappening->setText("Sheep man attacks you");
         ui->Healthbar_player->setValue(m_player->getBreed().getHealth());
         round++;
    }else if(round==4){
         m_player->takedmg(npc->attack());
         ui->WhatisHappening->setText("Sheep man attacks you");
          ui->Healthbar_player->setValue(m_player->getBreed().getHealth());
          round++;
    }else if(round==5){
         m_player->takedmg(npc->attack());
         ui->WhatisHappening->setText("Sheep man attacks you");
         ui->Healthbar_player->setValue(m_player->getBreed().getHealth());
         round=1;
    }
}

/**
 * @brief Fight_Window::sheepfight
 * @details Das Kampfsystem verläuft nach einem Runden prinzip. Jede Weiter Runde macht der Gegner was anderes.
 * @details Sie greifen unter umständen auch den Player an und ziehem ihm Werte ab.
 */

void Fight_Window::bossfight()
{
    if(bossround==1){
         ui->WhatisHappening->setText("He get up and looks anticlimactic in your direction.");
         bossround++;
    }else if(bossround==2){
         ui->WhatisHappening->setText("Jiren open his Arms.");
         bossround++;
    }else if(bossround==3){
         ui->WhatisHappening->setText("Jiren smiles.");
         bossround++;
    }else if(bossround==4){
        ui->Jirenultrainstinct->show();
        ui->Jiren->hide();
         ui->WhatisHappening->setText("Jiren release his Power");
         bossround++;
    }else if(bossround==5){
         ui->WhatisHappening->setText("Jiren attacks you.");
         ui->Game_Over->setText("Jiren attacked you.\n"
                                "You lost.\n"
                                "This game end here. \n"
                                "Thank you for playing this Demo");
         ui->Game_Over->show();
    }
}
