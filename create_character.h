#ifndef CREATE_CHARACTER_H
#define CREATE_CHARACTER_H
#include "controller.h"
#include <QWidget>
#include <breed.h>
#include <weapon.h>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
namespace Ui {
class Create_Character;
}

class Create_Character : public QWidget
{
    Q_OBJECT

public:
    explicit Create_Character(QWidget *parent = 0);
    ~Create_Character();

signals:
    void send_Createt_Player(Controller *player);

public slots:


private slots:
    void on_apply_Button_clicked();

private:
    Ui::Create_Character *ui;
    bool m_hide;
    Controller *m_player;
    Weapon *weapon;
    Breed *breed;

};

#endif // CREATE_CHARACTER_H
