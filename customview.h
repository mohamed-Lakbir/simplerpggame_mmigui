#ifndef CUSTOMVIEW_H
#define CUSTOMVIEW_H
#include <QGraphicsView>
#include <QKeyEvent>
#include <QDebug>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
class CustomView: public QGraphicsView
{
public:
    CustomView(QWidget *parent=0);
protected:
    void keyPressEvent(QKeyEvent *event);
};

#endif // CUSTOMVIEW_H
