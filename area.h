#ifndef AREA_H
#define AREA_H

#include <QObject>
#include "QGraphicsItem"
#include "QGraphicsRectItem"
#include "QString"
#include <QMap>
#include <controller.h>
#include <QColor>
#include <QBrush>
#include <QPen>
#include <QList>
#include <cstdlib>
#include <ctime>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
class Area: public QObject
{
    Q_OBJECT
public:
    Area(QColor color, qreal pos_x, qreal pos_y, QObject* parent = nullptr);
    Area(QObject* parent = nullptr);
    ~Area();
    QGraphicsRectItem *rec() const;
    void setRec(QGraphicsRectItem *rec);




    int pos_x() const;
    void setPos_x(int pos_x);

    int pos_y() const;
    void setPos_y(int pos_y);

    bool player_is_here() const;
    void setPlayer_is_here(bool player_is_here);



    QString storytext() const;
    void setStorytext(const QString &storytext);

    void move(Area *area);
    QList<Controller *> controller_list() const;
    void setController_list(const QList<Controller *> &controller_list);

    QString key() const;
    void setKey(const QString &key);

    bool closed_door() const;
    void setClosed_door(bool closed_door);

    bool exit_is_here() const;
    void setExit_is_here(bool exit_is_here);

    QColor defaultColor() const;
    void setDefaultColor(const QColor &defaultColor);

    bool encounter() const;
    void setEncounter(bool encounter);

signals:

private:

    int
    m_pos_x,
    m_pos_y;

    QGraphicsRectItem *m_rec;

    int m_enviromentdmg;
    QString m_storytext;

    bool m_player_is_here;
    bool m_closed_door;
    bool m_exit_is_here;
    bool m_encounter;

    void opendoor(QString key);

    QList <Controller*> m_controller_list;

    QColor m_defaultColor;

    QString m_lock;





public slots:
};

#endif // AREA_H
