#ifndef GAME_OPTIONS_H
#define GAME_OPTIONS_H

#include <QObject>
#include <controller.h>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
class Game_Options : public QObject
{
    Q_OBJECT
public:
    explicit Game_Options(QObject *parent = nullptr);





    Controller *player() const;
signals:


public slots:
    void setPlayer(Controller *player);

private:
    Controller *m_player;
};

#endif // GAME_OPTIONS_H
