#include "controller.h"

Controller::Controller()
{

}

/**
 * @brief Controller::Controller
 * @param name Setzt den Namen des Speicherstandes oder auch des Bots
 * @param am_I_Human Ein Boolwert, der bestimmt, ob dieses Objekt ein Mensch oder ein Bot ist.
 * @param breed Setzt den Breed dieses Objekt.
 */
Controller::Controller(QString name, bool am_I_Human, const Breed &breed)
{
    this->m_Name=name;
    this->setAm_I_Human(am_I_Human);
    this->breed=breed;
}

Controller::~Controller()
{

}

Breed Controller::getBreed() const
{
    return breed;
}

void Controller::setBreed(const Breed &breed)
{
    this->breed = breed;
}
/**
 * @brief Controller::getMyBreed_as_String
 * @details Diese Methode findet heraus, was für eine Rasse dieser Controller had.
 */
void Controller::getMyBreed_as_String()
{
    QString breed;
    if(this->breed.getWhat_is_my_Breed()==Breed::BREED::DWARF)
    {
        breed="Dwarf";
    }
    if(this->breed.getWhat_is_my_Breed()==Breed::BREED::ELF)
    {

        breed="Elf";
    }
    if (this->breed.getWhat_is_my_Breed()==Breed::BREED::HUMAN)
    {
        breed="Human";
    }
}


QString Controller::Name() const
{
    return m_Name;
}

void Controller::setName(const QString &Name)
{
    m_Name = Name;
}

bool Controller::getAm_I_Human() const
{
    return am_I_Human;
}

void Controller::setAm_I_Human(bool value)
{
    am_I_Human = value;
}

QString Controller::getKey() const
{
    return m_key;
}

void Controller::setKey(const QString &key)
{
    m_key = key;
}

QString Controller::toString()
{
    return  "Player:             "+this->m_Name+"\n"
           +"Character Name:     "+this->breed.getName()+"\n"
//         +"breed:              "+this->getMyBreed_as_String()+"/n"
           +"Age:                "+QString::number(this->breed.getAge())+"\n"
           +"Hp:                 "+QString::number(this->breed.getHealth())+"\n"
           +"Mana:               "+QString::number(this->breed.getMagic())+"\n"
           +"Attack:             "+QString::number(this->breed.getAttack())+"\n"
           +"Defense:            "+QString::number(this->breed.getBlock());

}

int Controller::getMy_position() const
{
    return my_position;
}

void Controller::setMy_position(int value)
{
    my_position = value;
}
/**
 * @brief Controller::heal
 * @param heal Der Heal Wert, mit dem der Charakter gehealt wird.
 * @details Diese Methode heilt den Charakter im Kampf.
 */
void Controller::heal(int heal)
{
    if(heal+breed.getHealth()<100){
    breed.setHealth(breed.getHealth()+heal);
    }else{
        breed.setHealth(100);
    }
}

/**
 * @brief Controller::attack
 * @return Der return Wert ist ein Wert, der den Charakter Angriffskraft des Breedes bestimmt.
 */
int Controller::attack()
{
    return breed.getAttack();
}

/**
 * @brief Controller::takedmg
 * @param dmg der Wert, dem der Charakter vom Leben abgezogen wird.
 * @details Diese Methode bestimmt, wieviel schaden der Charakter bekommen. Warum auch immer.
 */
void Controller::takedmg(int dmg)
{
    this->breed.setHealth(breed.getHealth()-dmg);
}

QString Controller::getName() const
{
    return m_Name;
}

