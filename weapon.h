#ifndef WEAPON_H
#define WEAPON_H

#include <QObject>
#include <QString>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
class Weapon
{

public:
    Weapon();
    Weapon(QString name,int dmg);


    int dmg() const;
    void setDmg(int dmg);

private:
    int m_dmg;
    QString name;


};

#endif // WEAPON_H
