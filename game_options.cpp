#include "game_options.h"

Game_Options::Game_Options(QObject *parent) : QObject(parent)
{
    m_player=new Controller();
}

Controller *Game_Options::player() const
{
    return m_player;
}

void Game_Options::setPlayer(Controller *player)
{
    m_player = player;
}

