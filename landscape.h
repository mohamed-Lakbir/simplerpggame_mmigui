#ifndef LANDSCAPE_H
#define LANDSCAPE_H
#include "area.h"
#include "qgraphicsscene.h"
#include <QMap>
#include <QObject>
#include <iterator>
#include <QBrush>
#include <controller.h>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
class landscape : public QMap <int,Area*>
{
public:
    landscape();

    void setStartpoint(int i, Controller *player);


    void createExit(int i);

    void leaveArea(int i);

    void moveArea(int i);

    void movePlayerfrom_to(int from, int to);

    bool checkexit(int i);

    bool checkstart(int i);

    landscape movePlayerfrom_to2(landscape *m_Landscape, int from, int to, Controller *m_player);


    void insertArea(QColor color, int i);


    void addtoscene(QGraphicsScene *scene);


    int length() const;
    void setLength(int length);

    int start() const;
    void setStart(int start);

    int exit() const;
    void setExit(int exit);

    bool encounter() const;
    void setEncounter(bool encounter);

private:
    int m_length;
    int m_start;
    int m_exit;
    bool m_encounter;




};

#endif // LANDSCAPE_H
