#include "game_window.h"
#include "ui_game_window.h"
#include "QDebug"
#include "area.h"
#include "QMap"
#include "QColor"
#include "QBrush"
#include "QPen"
#include "QDebug"
#include "customview.h"
#include "QString"
/**
 * @brief Game_Window::Game_Window
 * @param parent
 * @details Hier werden alle Instanzvariablen initialisiert und die connections verbindet
 * @details Beim erzeugen dieses Konstrukters werdena auch die Spawnpunkte hier gesetzt, sowie das Bild erzeugt.
 */
Game_Window::Game_Window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Game_Window)
{
    ui->setupUi(this);
    m_scene   = new QGraphicsScene();
    m_player  = new Controller();
    m_dungeon = new landscape();
    m_start   = new landscape();
    m_second  = new landscape();
    fight     = new Fight_Window(this);
    fight->hide();
    connect(ui->widget,SIGNAL(send_Createt_Player(Controller*)),this,SLOT(setPlayer(Controller*)));
    connect(this,SIGNAL(playerChanged(Controller*)),fight,SLOT(setPlayer(Controller*)));
    connect(this,SIGNAL(fightstarts(bool)),fight,SLOT(bossfightAction(bool)));

    ui->Tipps->setText("Tipp:Steure das Spielgeschen mit den Pfeiltasten \n"
                       "Tipp:Im Kampf nutze die Maus\n"
                       "Tipp:Der Rote Bereich bis du, der schwarze dein Ziel \n"
                       "Tipp:Versuche deinen Gegner auf 0% zu kriegen und versuche dein Leben über 0% zu halten.");

    m_start->insertArea(Qt::green,25);
    m_dungeon->insertArea(Qt::darkGray,25);
    m_second->insertArea(Qt::green,25);
    ui->Game_view->setScene(m_scene);




    m_start->setStartpoint(22,m_player);

    m_start->createExit(2);
    m_dungeon->createExit(0);
//    m_second->createExit(24);

    enter_Start();

}

Game_Window::~Game_Window()
{
;
    delete ui;
}

/**
 * @brief Game_Window::on_return_to_the_Menu_clicked
 * @details Sorgt dafür, das man wieder ins Hauptmenü landet.
 */
void Game_Window::on_return_to_the_Menu_clicked()
{
    this->close();
    emit this->i_am_finshed();
}

/**
 * @brief Game_Window::setPlayer
 * @param player der Verändete player.
 * @details Der Player wird hier gesetzt. Dies ist ein public slot.
 */
void Game_Window::setPlayer(Controller *player)
{
    int position=m_player->getMy_position();

    m_player = player;
    this->updatePlayer();
    m_player->setMy_position(position);
}

/**
 * @brief Game_Window::updateScene
 * @param m_Landscape Die neue Landschaft, die auf die scene Gesetzt werden soll
 * @details aktuallisiert die scene.
 */
void Game_Window::updateScene(landscape m_Landscape)
{
    m_Landscape.addtoscene(m_scene);
}

/**
 * @brief Game_Window::updatePlayer
 * @details Wenn der Player sich irgendwie verändert, wie diese Methode aufgerufen.
 */
void Game_Window::updatePlayer()
{
    ui->Character_Info->clear();
    ui->Character_Info->setText(m_player->toString());
}

/**
 * @brief Game_Window::moveUp
 * @details Dies ist der Button der dafür sorgt, das der Spieler sich bewegt.
 * @details Hier werden auch Events bestimmt und auch auch Texte bestimmt, die erscheinen sollen, wenn ein bestimmtes Feld betreten werden soll.
 * @details Diese Methoden müssten noch in einige viele Methoden unterteilt werden.
 */
void Game_Window::moveUp()
{

    if(m_player->getMy_position()-5>=0)
    {
      if(start==true){
            m_start->moveArea(m_player->getMy_position()-5);
            m_start->leaveArea(m_player->getMy_position());
            m_player->setMy_position(m_player->getMy_position()-5);
            m_start->addtoscene(m_scene);

            ui->area_Text->clear();
            ui->area_Text->setText("The sun shines, the lawn is green, the Kids are playing. \n "
                                   "What a loving day.\n"
                                   "What is this building other there? Yesterday there was no building here.\n"
                                   "I should go closer.");
            if(m_player->getMy_position()==14
               ||m_player->getMy_position()==23
               ||m_player->getMy_position()==18){
               emit playerChanged(m_player);
               emit fightstarts(false);
            }

            if(m_player->getMy_position()==1
                    ||m_player->getMy_position()==7
                    ||m_player->getMy_position()==3){
                ui->area_Text->clear();
                ui->area_Text->setText("This building looks creepy.\n"
                                       "I shoudnt go in there.");

            }

            this->updatePlayer();
            if(m_player->getMy_position()==m_start->exit()){
                enter_Dungeon();
                m_dungeon->setStartpoint(24,m_player);
            }
        }

     }
     else{
         ui->area_Text->clear();
         ui->area_Text->setText("I cant go any further in that direction, a wall obstruct me. ");
        }

        if(m_player->getMy_position()-5>=0){
            if(this->dungeon==true)
            {
                m_dungeon->moveArea(m_player->getMy_position()-5);
                m_dungeon->leaveArea(m_player->getMy_position());
                m_player->setMy_position(m_player->getMy_position()-5);
                m_dungeon->addtoscene(m_scene);

                if(m_player->getMy_position()==14
                   ||m_player->getMy_position()==23
                   ||m_player->getMy_position()==28){
                   emit playerChanged(m_player);
                   emit fightstarts(false);
                }
                if(m_player->getMy_position()==1
                   ||m_player->getMy_position()==6
                   ||m_player->getMy_position()==5){
                ui->area_Text->setText("You feel, that something horrific dangerous is close to you.");
                }
                this->updatePlayer();

                if(m_player->getMy_position()==m_dungeon->exit()){
                    emit playerChanged(m_player);
                    emit fightstarts(true);
                }
            }
        }else{
              ui->area_Text->clear();
              ui->area_Text->setText("I cant go any further in that direction, a wall obstruct me. ");
    }
    if(m_start->encounter()==true||m_dungeon->encounter()==true)
    {
        qDebug()<<"encounter starts";
        m_start->setEncounter(false);
        m_dungeon->setEncounter(false);
     }
  qDebug()<<QString::number(m_player->getMy_position());
}

/**
 * @brief Game_Window::moveUp
 * @details Dies ist der Button der dafür sorgt, das der Spieler sich bewegt.
 * @details Hier werden auch Events bestimmt und auch auch Texte bestimmt, die erscheinen sollen, wenn ein bestimmtes Feld betreten werden soll.
 * @details Diese Methoden müssten noch in einige viele Methoden unterteilt werden.
 */
void Game_Window::moveDown()
{
     if(m_player->getMy_position()+5<=m_start->length())
     {
        if(start==true){
            m_start->moveArea(m_player->getMy_position()+5);
            m_start->leaveArea(m_player->getMy_position());
            m_player->setMy_position(m_player->getMy_position()+5);
            m_start->addtoscene(m_scene);

            if(m_player->getMy_position()==1
                    ||m_player->getMy_position()==7
                    ||m_player->getMy_position()==3){
                ui->area_Text->clear();
                ui->area_Text->setText("This building looks creepy.\n"
                                       "I shoudnt go in there.");

            }

            ui->area_Text->clear();
            ui->area_Text->setText("The sun shines, the lawn is green, the Kids are playing. \n "
                                   "What a loving day.\n"
                                   "What is this building other there? Yesterday there was no building here.\n"
                                   "I should go closer.");

            this->updatePlayer();
            if(m_player->getMy_position()==m_start->exit())
            {
                enter_Dungeon();
                m_dungeon->setStartpoint(24,m_player);
            }
        }

     }else
     {
            ui->area_Text->clear();
            ui->area_Text->setText("I cant go any further in that direction, a wall obstruct me. ");
     }

     if(m_player->getMy_position()+5<=m_dungeon->length()){
        if(this->dungeon==true)
        {
            m_dungeon->moveArea(m_player->getMy_position()+5);
            m_dungeon->leaveArea(m_player->getMy_position());
            m_player->setMy_position(m_player->getMy_position()+5);
            m_dungeon->addtoscene(m_scene);

            if(m_player->getMy_position()==14
               ||m_player->getMy_position()==23
               ||m_player->getMy_position()==18){
               emit playerChanged(m_player);
               emit fightstarts(false);
            }


            if(m_player->getMy_position()==1
               ||m_player->getMy_position()==6
               ||m_player->getMy_position()==5){
            ui->area_Text->setText("You feel, that something horrific dangerous is close to you.");
            }

            this->updatePlayer();

            if(m_player->getMy_position()==m_dungeon->exit()){
                emit playerChanged(m_player);
                emit fightstarts(true);
            }
        }else{
            ui->area_Text->clear();
            ui->area_Text->setText("I cant go any further in that direction, a wall obstruct me. ");
     }
   }
   if(m_start->encounter()==true||m_dungeon->encounter()==true)
    {
      qDebug()<<"encounter starts";
      m_start->setEncounter(false);
      m_dungeon->setEncounter(false);
     }
  qDebug()<<QString::number(m_player->getMy_position());
}

/**
 * @brief Game_Window::moveUp
 * @details Dies ist der Button der dafür sorgt, das der Spieler sich bewegt.
 * @details Hier werden auch Events bestimmt und auch auch Texte bestimmt, die erscheinen sollen, wenn ein bestimmtes Feld betreten werden soll.
 * @details Diese Methoden müssten noch in einige viele Methoden unterteilt werden.
 */
void Game_Window::moveRight()
{
    if(m_player->getMy_position()%10==4||m_player->getMy_position()%10==9){
        ui->area_Text->clear();
        ui->area_Text->setText("I shoudnt go anyfurther here. Its too dangerous there.");
    }else if(start==true){

            m_start->moveArea(m_player->getMy_position()+1);
            m_start->leaveArea(m_player->getMy_position());
            m_player->setMy_position(m_player->getMy_position()+1);
            m_start->addtoscene(m_scene);

            ui->area_Text->clear();
            ui->area_Text->setText("The sun shines, the lawn is green, the Kids are playing. \n "
                                   "What a loving day.\n"
                                   "What is this building other there? Yesterday there was no building here.\n"
                                   "I should go closer.");

            if(m_player->getMy_position()==1
                    ||m_player->getMy_position()==7
                    ||m_player->getMy_position()==3){
                ui->area_Text->clear();
                ui->area_Text->setText("This building looks creepy.\n"
                                       "I shoudnt go in there.");

            }

            this->updatePlayer();
            if(m_player->getMy_position()==m_start->exit()){
                enter_Dungeon();
                m_dungeon->setStartpoint(24,m_player);
            }
        }else if(this->dungeon==true)
     {
            m_dungeon->moveArea(m_player->getMy_position()+1);
            m_dungeon->leaveArea(m_player->getMy_position());
            m_player->setMy_position(m_player->getMy_position()+1);
            m_dungeon->addtoscene(m_scene);


            if(m_player->getMy_position()==14
               ||m_player->getMy_position()==23
               ||m_player->getMy_position()==18){
               emit playerChanged(m_player);
               emit fightstarts(false);
            }

            if(m_player->getMy_position()==1
               ||m_player->getMy_position()==6
               ||m_player->getMy_position()==5){
            ui->area_Text->setText("You feel, that something horrific dangerous is close to you.");
            }

            this->updatePlayer();

            if(m_player->getMy_position()==m_dungeon->exit()){
                emit playerChanged(m_player);
                emit fightstarts(true);
            }
    }


    if(m_start->encounter()==true||m_dungeon->encounter()==true)
    {
      qDebug()<<"encounter starts";
      m_start->setEncounter(false);
      m_dungeon->setEncounter(false);
    }
    qDebug()<<QString::number(m_player->getMy_position());
}

/**
 * @brief Game_Window::moveUp
 * @details Dies ist der Button der dafür sorgt, das der Spieler sich bewegt.
 * @details Hier werden auch Events bestimmt und auch auch Texte bestimmt, die erscheinen sollen, wenn ein bestimmtes Feld betreten werden soll.
 * @details Diese Methoden müssten noch in einige viele Methoden unterteilt werden.
 */
void Game_Window::moveleft()
{
    if(m_player->getMy_position()%10==0||m_player->getMy_position()%10==5){
        ui->area_Text->clear();
        ui->area_Text->setText("I shoudnt go anyfurther here. Its too dangerous there.");
    }else if(start==true)
        {
            m_start->moveArea(m_player->getMy_position()-1);
            m_start->leaveArea(m_player->getMy_position());
            m_player->setMy_position(m_player->getMy_position()-1);
            m_start->addtoscene(m_scene);
            qDebug()<<QString::number(m_player->getMy_position());
            ui->area_Text->clear();
            ui->area_Text->setText("The sun shines, the lawn is green, the Kids are playing. \n "
                                   "What a loving day.\n"
                                   "What is this building other there? Yesterday there was no building here.\n"
                                   "I should go closer.");

            if(m_player->getMy_position()==1
                    ||m_player->getMy_position()==7
                    ||m_player->getMy_position()==3){
                ui->area_Text->clear();
                ui->area_Text->setText("This building looks creepy.\n"
                                       "I shoudnt go in there.");

            }

            this->updatePlayer();

            if(m_player->getMy_position()==m_start->exit()){
                enter_Dungeon();
                m_dungeon->setStartpoint(24,m_player);
             }
         }

        else if(this->dungeon==true){
                m_dungeon->moveArea(m_player->getMy_position()-1);
                m_dungeon->leaveArea(m_player->getMy_position());
                m_player->setMy_position(m_player->getMy_position()-1);
                m_dungeon->addtoscene(m_scene);
                ui->area_Text->clear();
                ui->area_Text->setText("Its smells here realy bad. \n"
                                       "Everything is gray and bleak in here.\n"
                                       "The entrance closed. There is no way back.");

                if(m_player->getMy_position()==14
                   ||m_player->getMy_position()==23
                   ||m_player->getMy_position()==18){
                   emit playerChanged(m_player);
                   emit fightstarts(false);
                }


                if(m_player->getMy_position()==1
                   ||m_player->getMy_position()==6
                   ||m_player->getMy_position()==5){
                ui->area_Text->clear();
                ui->area_Text->setText("You feel, that something horrific dangerous is close to you.");
                }


                this->updatePlayer();

                if(m_player->getMy_position()==m_dungeon->exit()){
                    emit playerChanged(m_player);
                    emit fightstarts(true);
                }
    }

  if(m_start->encounter()==true||m_dungeon->encounter()==true)
  {
    qDebug()<<"encounter starts";
    m_start->setEncounter(false);
    m_dungeon->setEncounter(false);
  }
  qDebug()<<QString::number(m_player->getMy_position());
}

Controller *Game_Window::player() const
{
    return m_player;
}

void Game_Window::enter_Start()
{
    m_start->addtoscene(m_scene);
    start=true;
    dungeon=false;
}

/**
 * @brief Game_Window::enter_Second
 * @details sorgt dafür, die Map second erzeugt wird auf die Graphicscene
 */
void Game_Window::enter_Second()
{
    m_second->addtoscene(m_scene);
}
/**
 * @brief Game_Window::enter_Dungeon
 * @details sorgt dafür, das das dungeon erzeugt wird auf der Graphicscene
 */
void Game_Window::enter_Dungeon()
{
    start=false;
    dungeon=true;
    m_dungeon->addtoscene(m_scene);
}


/**
 * @brief Game_Window::on_move_down_clicked
 * @details Der Button der die Bewegungs methoden aufruft.
 */
void Game_Window::on_move_down_clicked()
{
    this->updatePlayer();
    this->moveDown();
}

/**
 * @brief Game_Window::on_move_down_clicked
 * @details Der Button der die Bewegungs methoden aufruft.
 */
void Game_Window::on_move_left_clicked()
{
        this->updatePlayer();
        moveleft();
}

/**
 * @brief Game_Window::on_move_down_clicked
 * @details Der Button der die Bewegungs methoden aufruft.
 */
void Game_Window::on_move_right_clicked()
{
      this->updatePlayer();
        moveRight();
}

/**
 * @brief Game_Window::on_move_down_clicked
 * @details Der Button der die Bewegungs methoden aufruft.
 */
void Game_Window::on_move_up_clicked()
{
    this->moveUp();
    this->updatePlayer();
}
