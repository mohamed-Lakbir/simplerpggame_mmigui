#include "credits.h"
#include "ui_credits.h"

Credits::Credits(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Credits)
{
    ui->setupUi(this);
}

Credits::~Credits()
{
    delete ui;
}



void Credits::on_Back_to_Menu_clicked()
{
    this->close();
    emit this->i_am_finshed();
}
