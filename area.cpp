#include "area.h"

Area::Area(QObject* parent):QObject(parent)
{

}
/**
 * @brief Area::Area
 * @param color setzt die Farbe des Bereiches
 * @param pos_x setzt die Position, in der das rec gelegt wird
 * @param pos_y setzt die Poisition, in der das rec gelegt wird
 * @param parent
 */
Area::Area(QColor color,qreal pos_x,qreal pos_y, QObject *parent):QObject (parent)
{
    m_pos_x=pos_x;
    m_pos_y=pos_y;
    m_rec = new QGraphicsRectItem();
    m_rec->setRect(pos_x,pos_y,100,100);
    m_rec->setBrush(QBrush(color));
    m_rec->setPen(QPen(Qt::black));
    this->m_player_is_here=false;
    this->m_closed_door=false;
    m_defaultColor= color;
    m_encounter=false;

    srand((int)time(0));
    int r = (rand()%100)+1;

    if (r<=20){
        m_encounter=true;
    }

//    m_controller_list=new QList<Controller*>();
}

Area::~Area()
{
    delete m_rec;
}

QGraphicsRectItem *Area::rec() const
{
    return m_rec;
}

void Area::setRec(QGraphicsRectItem *rec)
{
    m_rec = rec;
}

int Area::pos_x() const
{
    return m_pos_x;
}

void Area::setPos_x(int pos_x)
{
    m_pos_x = pos_x;
    m_rec->setX(pos_x);
}

int Area::pos_y() const
{
    return m_pos_y;
}

void Area::setPos_y(int pos_y)
{
    m_pos_y = pos_y;
    m_rec->setY(pos_y);
}

bool Area::player_is_here() const
{
    return m_player_is_here;
}

void Area::setPlayer_is_here(bool player_is_here)
{
    m_player_is_here = player_is_here;
}


QString Area::storytext() const
{
    return m_storytext;
}

void Area::setStorytext(const QString &storytext)
{
    m_storytext = storytext;
}

/**
 * @brief Area::move
 * @details eine Methode, um den Player von einer Area zum nächsten Area zu bwegen.
 * @details Für die implementierung nicht genutzt.
 * @param area ist die Ziel Area, des Players.
 */
void Area::move(Area *area)
{
    area->controller_list().push_back(this->m_controller_list.last());
    area->rec()->setBrush(QBrush(Qt::yellow));
    area->rec()->setPen(QPen(Qt::black));
    area->setPlayer_is_here(true);
    rec()->setBrush(QBrush(m_defaultColor));
    m_controller_list.removeLast();
    m_player_is_here=false;
}

QList<Controller *> Area::controller_list() const
{
    return m_controller_list;
}

void Area::setController_list(const QList<Controller *> &controller_list)
{
    m_controller_list = controller_list;
}

bool Area::closed_door() const
{
    return m_closed_door;
}

void Area::setClosed_door(bool closed_door)
{
    m_closed_door = closed_door;
}

bool Area::exit_is_here() const
{
    return m_exit_is_here;
}

void Area::setExit_is_here(bool exit_is_here)
{
    m_exit_is_here = exit_is_here;
}

void Area::opendoor(QString key)
{
    if(key==m_lock){
        m_closed_door=false;
    }
}

QColor Area::defaultColor() const
{
    return m_defaultColor;
}

void Area::setDefaultColor(const QColor &defaultColor)
{
    m_defaultColor = defaultColor;
}

bool Area::encounter() const
{
    return m_encounter;
}

void Area::setEncounter(bool encounter)
{
    m_encounter = encounter;
}
