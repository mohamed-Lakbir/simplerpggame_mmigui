#ifndef CREDITS_H
#define CREDITS_H

#include <QDialog>


/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
namespace Ui {
class Credits;
}

class Credits : public QDialog
{
    Q_OBJECT

public:
    explicit Credits(QWidget *parent = 0);
    ~Credits();
signals:
    void i_am_finshed();

private slots:

    void on_Back_to_Menu_clicked();

private:
    Ui::Credits *ui;
};

#endif // CREDITS_H
