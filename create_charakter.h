#ifndef CHREATE_CHARAKTER_H
#define CHREATE_CHARAKTER_H

#include <QWidget>

namespace Ui {
class Create_Charakter;
}

class Create_Charakter : public QWidget
{
    Q_OBJECT

public:
    explicit Create_Charakter(QWidget *parent = 0);
    ~Create_Charakter();

private:
    Ui::Create_Charakter *ui;
};

#endif // CHREATE_CHARAKTER_H
