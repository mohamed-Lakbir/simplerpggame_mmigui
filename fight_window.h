#ifndef FIGHT_WINDOW_H
#define FIGHT_WINDOW_H
#include <controller.h>
#include <breed.h>


#include <QDialog>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
namespace Ui {
class Fight_Window;
}

class Fight_Window : public QDialog
{
    Q_OBJECT

public:
    explicit Fight_Window(QWidget *parent = 0);
    ~Fight_Window();



    bool getAm_I_an_Boss() const;
    void setAm_I_an_Boss(bool am_I_an_Boss);


    Controller *getPlayer() const;


    Controller *getNpc() const;
    void setNpc(Controller *value);

public slots:

    void bossfightAction(bool bossfight);

    void setPlayer(Controller *player);


signals:
    void i_am_finshed();
    void afterfight(Controller *player);

private slots:
    void on_pushButton_clicked();

    void on_Attack_clicked();

    void on_Heal_clicked();

    void on_Continue_clicked();

    void bossfight();

    void sheepfight();
private:
    Ui::Fight_Window *ui;

    Controller *m_player;
    Controller *npc;
    int         round;
    int         bossround;
    bool        m_am_I_an_Boss;
};

#endif // FIGHT_WINDOW_H
