#include "weapon.h"
#include <random>
#include <cstdlib>
#include <qdebug.h>
#include <QString>
#include <ctime>



std::random_device rd;
std::mt19937 rng(rd());
std::uniform_int_distribution<int> uni(0,10);

Weapon::Weapon()
{
    this->m_dmg=0;
}

/**
 * @brief Weapon::Weapon
 * @param name Der Name der Waffe
 * @param dmg  Die Angriffskraft, die die Waffe bekommen soll.
 * @details Erzeugt eine Waffe mit namen und einer bestimmten Angriffskraft sowie einen Bonus schaden, der Zufällig bestimmt wird.
 */
Weapon::Weapon(QString name, int dmg)
{
    this->name=name;
    this->m_dmg=dmg;
    if(dmg>0){
        srand((int)time(0));
        m_dmg=m_dmg+((rand()%10)+1);
    }
}

int Weapon::dmg() const
{
    return m_dmg;
}

void Weapon::setDmg(int dmg)
{
    m_dmg = dmg;
}
