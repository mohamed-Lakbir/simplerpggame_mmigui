#include "mainwindow.h"
#include "ui_mainwindow.h"
/**
 * @brief MainWindow::MainWindow
 * @details erzeugt alle notwendigung widget und macht alle connection um den mechanismus auszulösen, der die Fenster öffnet und schließt.
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->game= new Game_Window(this);
    this->options=new Game_Options();
    this->cr_ch=new Create_Character();
    this->help= new Instructions(this);
    this->credits= new Credits(this);
    connect(game,SIGNAL(i_am_finshed()),this,SLOT(show_youself()));

//    connect(this->game,SIGNAL(playerChanged(Controller)),this->options,SLOT(setPlayer(Controller)));
    connect(help,SIGNAL(i_am_finshed()),this,SLOT(show_youself()));
    connect(credits,SIGNAL(i_am_finshed()),this,SLOT(show_youself()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief MainWindow::show_youself
 * @details erzeugt das das Hauptmenu
 */
void MainWindow::show_youself()
{
    this->show();
}

/**
 * @brief MainWindow::on_close_Button_clicked
 * @details schließt das Programm.
 */
void MainWindow::on_close_Button_clicked()
{
    this->close();
}

/**
 * @brief MainWindow::on_start_Button_clicked
 * @details Startet das SPiel.
 */
void MainWindow::on_start_Button_clicked()
{
    this->hide();
    game->show();
}

/**
 * @brief MainWindow::on_instructions_Button_clicked
 * @details Erzeugt das Instruction fenster.
 */
void MainWindow::on_instructions_Button_clicked()
{
    this->hide();
    help->show();
}

/**
 * @brief MainWindow::on_Credits_Button_clicked
 * @details Erzeugt die Credits.
 */
void MainWindow::on_Credits_Button_clicked()
{
    this->hide();
    credits->show();
}
