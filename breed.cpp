#include "breed.h"

Breed::Breed()
{

}

/**
 * @brief Breed::Breed
 * @param name Setzt den Namen des Charakters
 * @param age  Setzt das Alter des Charakters
 * @param health Setzt die Gesundheit des Charakters
 * @param magic Setzt die Magie werte des Charakters
 * @param block Setzt die Blockwerte des Charakters
 * @param heal  Setzt die Heal fähigkeit des Charakters
 * @param attack Setzt die Angrifsskraft des Charakters
 * @param waepon Setzt die Waffe des Charaktes
 */
Breed::Breed(QString name, int age, int health, int magic, int block, int heal, int attack, const Weapon &waepon)
{
    this->m_name=name;
    this->m_age=age;
    this->m_health=health;
    this->m_magic=magic;
    this->m_block=block;
    this->m_heal=heal;
    this->m_weapon=waepon;
    this->m_attack=attack+this->m_weapon.dmg();
}

QString Breed::getName() const
{
    return m_name;
}

void Breed::setName(const QString &name)
{
    m_name = name;
}

int Breed::getAge() const
{
    return m_age;
}

void Breed::setAge(int age)
{
    m_age = age;
}

int Breed::getHealth() const
{
    return m_health;
}

void Breed::setHealth(int health)
{
    m_health = health;
}

int Breed::getBlock() const
{
    return m_block;
}

void Breed::setBlock(int block)
{
    m_block = block;
}


void Breed::getDamage(int damage)
{

}

int Breed::getAttack() const
{
    return m_attack;
}

/**
 * @brief Breed::setAttack
 * @details Die Angriffskraft wird durch den unbewaffneteten Wert mit der Waffe berechnet.
 * @param attack
 */
void Breed::setAttack(int attack)
{
    m_attack = attack;
    this->m_attack=m_attack+this->m_weapon.dmg();
}

int Breed::getHeal() const
{
    return m_heal;
}

void Breed::setHeal(int heal)
{
    m_heal = heal;
}

int Breed::getMagic() const
{
    return m_magic;
}

void Breed::setMagic(int magic)
{
    m_magic = magic;
}

Weapon Breed::getWeapon() const
{
    return m_weapon;
}

/**
 * @brief Breed::setWeapon
 * @details Beim setzen der Waffe erhöt sich automatisch auch die Angriffskraft.
 * @param weapon
 */
void Breed::setWeapon(const Weapon &weapon)
{
    m_weapon = weapon;
    this->m_attack=m_attack+this->m_weapon.dmg();
}



void Breed::setWhat_is_my_Breed(const BREED &value)
{
    what_is_my_Breed = value;
}


Breed::BREED Breed::getWhat_is_my_Breed() const
{
    return this->what_is_my_Breed;
}
