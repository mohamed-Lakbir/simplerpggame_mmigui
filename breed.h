#ifndef BREED_H
#define BREED_H
#include <QObject>
#include <weapon.h>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
class Breed
{
public:

    Breed();

    Breed(QString name, int age, int health, int magic, int block, int heal,int attack, const Weapon &waepon);

    QString getName() const;
    void setName(const QString &name);

    int getAge() const;
    void setAge(int age);

    int getHealth() const;
    void setHealth(int health);

    int getArrow() const;
    void setArrow(int arrow);

    int getBlock() const;
    void setBlock(int block);


    enum BREED{ELF,DWARF,HUMAN};
    BREED getWhat_is_my_Breed() const;
    void setWhat_is_my_Breed(const BREED &value);

    Weapon getWeapon() const;
    void setWeapon(const Weapon &weapon);

    int getMagic() const;
    void setMagic(int magic);

    int getHeal() const;
    void setHeal(int heal);

    int getAttack() const;
    void setAttack(int attack);

signals:
    void makeDamage(int damage);

public slots:
    void getDamage(int damage);

private:

    QString m_name;
    int m_attack;
    int m_age;
    int m_health;
    int m_magic;
    int m_block;
    int m_heal;
    Breed::BREED what_is_my_Breed;
    Weapon m_weapon;
};

#endif // BREED_H
