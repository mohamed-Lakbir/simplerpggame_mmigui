#ifndef GAME_WINDOW_H
#define GAME_WINDOW_H
#include "controller.h"
#include <QDialog>
#include <landscape.h>
#include <QGraphicsScene>
#include <customgraphicscene.h>
#include <fight_window.h>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
namespace Ui {
class Game_Window;
}

class Game_Window : public QDialog
{
    Q_OBJECT

public:
    explicit Game_Window(QWidget *parent = 0);
    ~Game_Window();


    Controller *player() const;

    void enter_Start();
    void enter_Second();
    void enter_Dungeon();

    void updateScene(landscape m_Landscape);
    void updatePlayer();

    void moveUp();
    void moveDown();

    void moveRight();
    void moveleft();



public slots:
    //    void setPlayer(Controller &player);
    void setPlayer(Controller *player);


signals:
    void i_am_finshed();
    void playerChanged(Controller *player);
    void fightstarts(bool bossfight);


private slots:

    void on_return_to_the_Menu_clicked();

    void on_move_down_clicked();

    void on_move_left_clicked();

    void on_move_right_clicked();

    void on_move_up_clicked();

private:
    Ui::Game_Window *ui;
    QGraphicsScene  *m_scene;
    Controller      *m_player;
    landscape       *m_start;
    landscape       *m_second;
    landscape       *m_dungeon;
    Fight_Window    *fight;
    bool            start;
    bool            dungeon;

};

#endif // GAME_WINDOW_H
