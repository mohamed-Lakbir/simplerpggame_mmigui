#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "breed.h"
#include "QString"
#include "weapon.h"
#include <QObject>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
class Controller
{

public:
    Controller();
    Controller(QString name, bool am_I_Human, const Breed &breed);

    ~Controller();



    Breed getBreed() const;
    void setBreed(const Breed &breed);

    void getMyBreed_as_String();

    QString getName() const;
    QString Name() const;
    void setName(const QString &Name);

    bool getAm_I_Human() const;
    void setAm_I_Human(bool value);



    QString getKey() const;
    void setKey(const QString &key);

    QString toString();

    int getMy_position() const;
    void setMy_position(int value);

    void heal(int heal);

    int attack();
    void takedmg(int dmg);



private:
//    Q_DISABLE_COPY(Controller)

    Breed breed;

    QString m_Name;
    QString m_key;

    bool am_I_Human;

    int my_position;


};

#endif // CONTROLLER_H
