#include "landscape.h"
#include "qdebug.h"
#include "QString"

landscape::landscape()
{
    m_encounter=false;
}

/**
  * @brief landscape::setStartpoint
  * @param i Dies ist der Punkt, wo der Controller seinen Startpunkt haben soll.
  * @param player Dies ist der Controller, der auf das Feld gesetzt werden soll.
  * @details Dies ist eine Methode die dafür sorgen soll, das ein Controller an einer bestimmten stelle einen Startpunkt bestitzt.
  */
 void landscape::setStartpoint(int i,Controller *player )
{
    QMap<int,Area*>::const_iterator iter=this->constBegin();
    int b=0;
    while(iter != this->constEnd()){
        if(i==b){
           iter.value()->rec()->setBrush(QBrush(Qt::red));
           iter.value()->setPlayer_is_here(true);
           player->setMy_position(i);
           qDebug()<<(player->getMy_position());
           iter.value()->setEncounter(false);
        }
        b++;
        iter++;
    }
    m_start=i;
 }


 /**
 * @brief landscape::createExit
 * @param i Ist die Stelle, an der ein Ausgang auf der Karte erzeugt werden soll.
 * @details Diese Methode erstellt an einer bestimmten stelle einen Ausgang, der dann als Punkt gespeichert wird, das sich hier ein Ausgang befindet.
 */
void landscape::createExit(int i)
{
    QMap<int,Area*>::const_iterator iter=this->constBegin();
    int b=0;
    while(iter != this->constEnd()){
        if(i==b){
           iter.value()->rec()->setBrush(QBrush(Qt::black));
           iter.value()->setDefaultColor(Qt::black);
           iter.value()->setExit_is_here(true);
           iter.value()->setEncounter(false);
        }
        b++;
        iter++;
    }
    m_exit=i;
}

/**
 * @brief landscape::leaveArea
 * @param i Die stelle, die der Player verlassen soll.
 */
void landscape::leaveArea(int i)
{
    QMap<int,Area*>::const_iterator iter=this->constBegin();
    int b=0;
    while(iter != this->constEnd()){
        if(i==b){
           iter.value()->setPlayer_is_here(false);
           iter.value()->rec()->setBrush(QBrush(iter.value()->defaultColor()));
        }
        b++;
        iter++;
    }
}

/**
 * @brief landscape::moveArea
 * @param i setzt die Stelle zu der sich der Player auf der Karte bewegen soll.
 * @details Ist eine methode die die Ziel Position des Players speichert.
 */
void landscape::moveArea(int i)
{
    QMap<int,Area*>::const_iterator iter=this->constBegin();
    int b=0;
    while(iter != this->constEnd()){
        if(i==b){
           iter.value()->rec()->setBrush(QBrush(Qt::red));
           iter.value()->setPlayer_is_here(true);
           if(iter.value()->encounter()==true){
               m_encounter=true;
           }
        }
        b++;
        iter++;
    }

}

/**
 * @brief landscape::movePlayerfrom_to
 * @param from der Punkt, den der Spieler verlässt (der aktuelle punkt)
 * @param to Das Ziel zu dem der Player soll
 * @details Dies ist eine vereinigung der Methoden moveArea und leave Area.
 */
void landscape::movePlayerfrom_to(int from, int to)
{
   this->moveArea(to);
   this->leaveArea(from);

}

/**
 * @brief landscape::checkexit
 * @param i Die Stelle, die überprüfen soll, ob ein Exist hier vorliegt
 * @return Bei True liegt hier ein Ausgang vor bei false liegt hier kein Ausgang vor
 * @details Diese Methode kann dafür genutzt werden, ob ein Spieler einen Ausgang in der scene getroffen hat.
 */
bool landscape::checkexit(int i)
{
    if(i==m_exit){
        return true;
    }else{
        return false;
    }

}

/**
 * @brief landscape::checkstart
 * @param i Die Stelle, die überprüfen soll, ob ein Eingang hier vorliegt
 * @return Bei True liegt hier ein Eingang vor bei false liegt hier kein Eingang vor
 * @details Diese Methode kann dafür genutzt werden, ob ein Spieler einen Eingang in der scene getroffen hat.
 */
bool landscape::checkstart(int i)
{
    if(i==m_start){
        return true;
    }else{
        return false;
    }
}


landscape landscape::movePlayerfrom_to2(landscape *m_Landscape,int from, int to, Controller *m_player)
{
    QMap<int,Area*>::const_iterator iter=this->constBegin();
    int i2=0;
    while(iter != this->constEnd()){
        if(i2==from){
           iter.value()->setPlayer_is_here(false);
           iter.value()->controller_list().removeLast();
           iter.value()->rec()->setBrush(QBrush(iter.value()->defaultColor()));

        }
        from++;
        iter++;
    }

    QMap<int,Area*>::const_iterator iter2=this->constBegin();
     int i=0;
    while(iter2 != this->constEnd()){
        if(i==to){
           iter2.value()->rec()->setBrush(QBrush(Qt::red));
           iter2.value()->controller_list().push_back(m_player);
           iter2.value()->setPlayer_is_here(true);
        }
        to++;
        iter2++;
    }
//    m_Landscape=this;
//    return m_Landscape;
}

/**
 * @brief landscape::insertArea
 * @param color Die Farbe der recs, die in der Liste gespeichert werden sollen.
 * @param i Die Anzahl der Recs, die in der liste gespeichert werden sollen.
 * @details Dies ist eine Methode die dafür sorgt, das eine bestimmte Anzahl von gleichen Recs in die Map gezeichnet werden soll.
 */
void landscape::insertArea(QColor color, int i)
{
    m_length=i-1;

    int y=0;
    int x =0;
    for(int b=1;b<i+1;b++){

        Area *area=new Area(color,x,y);

        this->insert(b,area);

        if(x==400){
            x=0;
            y=y+100;
        }else{
            x=x+100;
        }
    }
}

/**
 * @brief landscape::addtoscene
 * @param scene die scene, wo die items geaddet werden sollen
 * @details Diese Methode dient dazu, die rectangle items auf eine belibige scene zu zeichnen.
 */
void landscape::addtoscene(QGraphicsScene *scene)
{

    QMap<int,Area*>::const_iterator iter=this->constBegin();
    while(iter != this->constEnd()){
        scene->addItem(iter.value()->rec());
        iter++;
    }
}

int landscape::length() const
{
    return m_length;
}

void landscape::setLength(int length)
{
    m_length = length;
}

int landscape::start() const
{
    return m_start;
}

void landscape::setStart(int start)
{
    m_start = start;
}

int landscape::exit() const
{
    return m_exit;
}

void landscape::setExit(int exit)
{
    m_exit = exit;
}

bool landscape::encounter() const
{
    return m_encounter;
}

void landscape::setEncounter(bool encounter)
{
    m_encounter = encounter;
}



