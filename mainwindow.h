#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "fight_window.h"
#include "game_window.h"
#include "instructions.h"
#include "credits.h"
#include "create_character.h"
#include <game_options.h>

/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void show_youself();

private slots:

    void on_start_Button_clicked();

    void on_instructions_Button_clicked();


    void on_Credits_Button_clicked();

    void on_close_Button_clicked();


private:
    Ui::MainWindow *ui;
    Game_Window *game;
    Instructions *help;
    Credits *credits;

    Create_Character *cr_ch;

    Game_Options *options;



};

#endif // MAINWINDOW_H
