#include "create_character.h"
#include "ui_create_character.h"
#include <QMessageBox>
#include <QDebug>
#include <breed.h>
#include <QString>


Create_Character::Create_Character(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Create_Character)
{
    ui->setupUi(this);
//    m_player= new Controller();
}

Create_Character::~Create_Character()
{
    delete ui;

}

/**
 * @brief Create_Character::on_apply_Button_clicked
 * @details Wenn der Spieler seinen Charakter erstellt hat, wird der erstellte Charakter versendet.
 */
void Create_Character::on_apply_Button_clicked()
{
    this->m_player=new Controller(ui->player_name->text(),true,Breed(ui->character_Name->text(),18,100,100,10,10,10,Weapon("Unarmed",1)));
    emit send_Createt_Player(m_player);
    this->hide();

}

