#ifndef CUSTOMGRAPHICSCENE_H
#define CUSTOMGRAPHICSCENE_H
#include <QGraphicsScene>
#include <QKeyEvent>
#include <qdebug.h>
#include <QGraphicsView>


/**
 *@author Lakbir Mohamed
 *@Version 0.0.0.1 Alpha
 *@date 07.3.2018
 */
class CustomGraphicscene :public QGraphicsView
{
public:
    void keyPressEvent(QKeyEvent * event);
};

#endif // CUSTOMGRAPHICSCENE_H
