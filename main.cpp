#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

//    srand((int)time(0));
//    int i = 0;
//    while(i++ < 10) {
//        int r = (rand() % 10) + 1;
//        cout << r << " ";
//    }
    return a.exec();
}
